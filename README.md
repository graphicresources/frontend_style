# README #

Nueva guía de estilos, siguiendo nuestro diseño de Freepik Company, pero mucho más liviana.

### Cómo hacer una release ###

```bash
git flow feature start FREE-1234-add-new-component
git add .
git commit -m "Add new component"
git push
git flow finish

git checkout develop
git push
git describe (devuelve el último tag, ej: 1.3.0)
git flow release start 1.4.0 (siempre es X.Y.0)
git fetch origin master:master
git flow finish

git checkout develop
git push
git checkout master
git push
git push --tags (si no lo tienes automático)
``` 

### Instalación ###

* webpack.config.js (Nota: apuntar a node_modules correctamente desde cada proyecto!)
```js
resolve: {
    alias: {
        "frontend_style": ["../../../../../../node_modules/frontend_style"],
    },
```

* package.json
```json
"dependencies": {
    "frontend_style": "git+https://bitbucket.org/graphicresources/frontend_style#1.3.0"
}
```

* _mystylesmainfile.scss
```scss
@import '~frontend_style/scss/base';
```


### Actualizar iconos ###

1. Si se necesita cambiar algo en la configuración por defecto de los iconos (como el color por defecto, el ancho, alto...), debe editarse en el fichero `webfont.scss.hbs`.
2. Subir el nuevo icono en la carpeta /icons.
3. Ejecutar en el proyecto:
```bash
npm run webfont
```
4. Subir los ficheros que se han creado/editado (`webfont.woff2`, `icons.scss`...).

### Estilos ###

**Sizes:**

- `xs`
- `sm` 
- `md` 
- `lg` 
- `xl` 
- `xxl`

**Paddings/margins:**

- `.pd-{size}`
- `.pd-{size}-{position}`

- `.pd-xs` 
- `.pd-md-top` 
- `.pd-lg-left` 
- `.pd-xl-bottom` 
- `.mg-xs` 
- `.mg-sm-right`

**Buttons:**

- `.button`
- `.button--outline`
- `.button--with-icon`
- `.button--full-width`
